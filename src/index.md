---
title: "Statistique III (Printemps 2024)"
---

Voici un résumé de mes notes de cours. Quelques erreurs s'y sont peut-être glissées, mais de manière générale la plupart des choses devrait être juste.

* Pour le cours de première année, [mes notes sont disponibles ici](https://stat1.enbian.space).
* Pour le cours de statistiques II, [mes notes sont disponibles ici](https://stat2.enbian.space).

[![CC-BY-SA 4.0](by-sa.svg)](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)
