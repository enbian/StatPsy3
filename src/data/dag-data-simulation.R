# Définir la 'graine' aléatoire
set.seed(100)

# Taille de l'échantillon
N <- 1000

# Processus aléatoire à l'origine des variations de X
Ux <- rnorm(N)

# Valeurs de X
X <- Ux

# Processus aléatoire à l'origine d'une partie des variations de Y
Uy <- rnorm(N)

# Force de la relation entre X et Y
B <- 0.4

# Valeurs de Y
Y <- B*X + Uy

# Régression linéaire simple
summary(lm(Y ~ X))

# Génération aléatoire de A
set.seed(20)
N <- 5000
Ua <- rnorm(N)
A <- Ua

# Génération aléatoire de C
Uc <- rnorm(N)
C <- Uc

# Génération aléatoire de B
Ub <- rnorm(N)
B <- Ub + 0.4*A + 0.6*C

# Génération aléatoire de D
Ud <- rnorm(N)
D <- Ud + 0.5*B + 0.3*C

# Modèle de régression
m1 <- lm(D ~ A)
summary(m1)

# Modèle de régression
m2 <- lm(D ~ B + C)
summary(m2)
