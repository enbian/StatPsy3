# Bootstrap

!!! abstract "Introduction"

    Bootstrap est une méthode statistique permettant d'améliorer l'estimation de l'erreur standard, des intervalles de confiance, et d'autres informations.

## Introduction théorique

La méthode bootstrap consiste à sélectionner au hasard des données dans le tableau de données originales par **tirage avec remise** &mdash; c'est-à-dire que les mêmes lignes peuvent être tirées plusieurs fois de suite. 

La méthode permet donc de créer de nouveaux tableaux de données à partir du tableau original. Ces tableaux seront différents du tableau original, puisqu'il contiendra certaines données du tableau original plusieurs fois, et qu'il ne contiendra pas certaines autres données du tableau original. 

Cette procédure est ensuite reproduite des milliers de fois, afin de pouvoir améliorer les estimations.

!!! note "Note"

    L'estimation de la force de la relation ne changera pas avec l'utilisation de bootstrap. Cependant, les intervalles de confiance, l'erreur standard et d'autres estimations vont être grandement améliorées.
