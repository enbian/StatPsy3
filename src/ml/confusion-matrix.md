# Matrices de confusion (Confusion Matrix)

!!! abstract "Introduction"

    Une matrice de confusion permet de comparer les données prédites avec les données réelles (de test). Elle permet de calculer divers indicateurs de la performance des prédictions, tels que l'exactitude, la précision, etc.

## Introduction théorique

Lorsque l'on réalise une matrice de confusion, il faut définir une valeur "positive", qui correspond à celle que nous essayons de prédire.

La matrice ressemble à peu près au tableau ci-dessous:

| | **Ref 0** | **Ref 1** |
| --- | --- | --- |
| **Pred 0** | 102 | 14 |
| **Pred 1** | 23 | 193 |

En haut, les valeurs de référence (celles qui sont les valeurs réelles, qui existent dans nos données de test). À droite, les valeurs prédites par le modèle.

Si nous souhaitons prédire la valeur de `1`, alors nous définisssons `1` comme le groupe cible. `1` devient donc la classe positive (qui est le nom donné à la classe/au groupe que nous souhaitons prédire). Dans la matrice ci-dessus, il y a donc:

- **193 vrais positifs** (valeurs prédites positivement de manière correcte)
- **23 faux positifs** (valeurs prédites positivement alors qu'elles sont en réalité négatives)
- **102 vrais négatifs** (valeurs prédites négativement de manière correcte)
- **14 faux négatifs** (valeurs prédites négativement alors qu'elles sont en réalité positives)

Sur la base de ces informations, il est possible de calculer plusieurs indices quant à la performance du modèle.

## Indices de performance

Soit:

- TP le nombre de vrais positifs (_True Positives_)
- FP le nombre de faux négatifs (_False Positives_)
- TN le nombre de vrais négatifs (_True Negatives_)
- FN le nombre de faux négatifs (_False Negatives_)

La précision (_precision_ se calcule de la manière suivante):

<figure markdown>
![Precision = TP/(TP+FP)](../latex/precision.svg)
</figure>

Le rappel se calcule de la manière suivante:

<figure markdown>
![Recall = TP/(TP+FN)](../latex/recall.svg)
</figure>

Enfin, l'exactitude se calcule de la façon suivante:

<figure markdown>
![Accuracy = (TP+TN)/(TP+TN+FP+FN)](../latex/accuracy.svg)
</figure>

De manière générale, plus l'exactitude se rapproche de `1`, plus le modèle est bon.

!!! warning "L'exactitude est dépendante de la taille des échantillons"

    Si les groupes positifs et négatifs ne font pas la même taille, il faut utiliser un autre indice : **l'exactitude balancée** (_Balanced Accuracy_), qui est indépendante de la taille des échantillons.

## Application

=== "En laissant R décider du groupe cible" 

    ```R title="Syntaxe R"
    # Soit y la variable dépendante,
    # pred le vecteur des prédictions de y,
    # et data_testing le dataframe contenant les données de test
    library(caret)
    confusionMatrix(data=pred, reference=data_testing$y)
    ```

=== "En spécifiant le groupe cible"

    ```R title="Syntaxe R"
    # Soit y la variable dépendante,
    # pred le vecteur des prédictions de y,
    # data_testing le dataframe contenant les données de test,
    # et 'A' le groupe cible que nous souhaitons prédire
    library(caret)
    confusionMatrix(data=pred, reference=data_testing$y, positive="A")
    ```

## Exemple

!!! example "Exemple"

    Pour voir un exemple de l'utilisation des matrices de confusion, voir [l'exemple de l'utilisation des arbres de classification](../ml/classification-trees.md).
