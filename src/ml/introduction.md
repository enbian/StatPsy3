# Introduction au machine learning

## Introduction théorique

Le machine learning a pour but de prédire un résultat sur la base de variables prédictives. On peut voir la prédiction comme une fonction dont l'équation est la suivante:

<figure markdown>
![y = f(X)](../latex/prediction.svg)
</figure>

Avec:

- y la variable dépendante que l'on veut prédire
- f la fonction de prédiction (fonction cible)
- X l'ensemble de variables prédictives

Or, il est difficile de connaître exactement ce qu'est cette fonction f dans la réalité &mdash; autrement, il n'y aurait pas besoin de données. Le machine learning consiste donc à faire apprendre à un logiciel quelle valeur donner à la variable dépendante en fonction d'un ensemble de variables prédictives, sur la base de données qu'on lui fourni.

!!! tip "Information"
    La prédiction n'est donc pas forcément vraie : il s'agit simplement de la valeur la plus probable, sur la base des données avec lesquelles le logiciel a été entraîné. Si ce dernier a été entraîné sur des données biaisées, alors les prédictions seront également biaisées par rapport à la réalité.

## Une approche différente de la causalité

L'apprentissage automatique est donc une approche totalement différente de l'approche causale vue précédemment. Ici, le but n'est plus de tester une théorie et de trouver ce qui est vrai, mais de prédire quelque chose. Le seul critère pour évaluer si la prédiction est bonne est de savoir si celle-ci est utile. Il n'y a donc aucune question de causalité dans l'approche de prédiction.

Par rapport aux approches causales, l'apprentissage automatique a également l'avantage de ne pas reposer sur des suppositions de distributions normales ou de relations linéaires entre les variables.

Le problème du machine learning, cependant, est que l'algorithme est une forme de "boîte noire". Pour les arbres de décision, il est encore possible de savoir ce qu'il s'y passe, mais pour les autres algorithmes, tels que les forêts aléatoires, ce qu'il se passe à l'intérieur de l'algorithme est difficile à connaître précisément.
