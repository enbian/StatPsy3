# Introduction aux arbres de décision (decision trees)

!!! abstract "Introduction"
    Les arbres de décision (decision trees) sont un algorithme d'apprentissage automatique supervisé.

## Introduction théorique

Pour les arbres de la décision, &mdash; comme pour les régressions &mdash; la variable dépendante peut être numérique ou binaire.

- Pour prédire une VD numérique, on utilise un **arbre de régression**.
- Pour prédire une VD binaire, on utilise un **arbre de classification**.

Pour construire un arbre de décision, il faut tout d'abord commencer par "séparer" les données. Dans un modèle logistique comme celui ci-dessous, il convient donc de trouver la "limite" entre les deux niveaux de la VD, dans les valeurs que peut prendre le prédicteur.

<figure markdown>
![Régression logistique](../graphics/ctree-theory-1.png){ width="450" }
</figure>

Tout d'abord, il semble que les valeurs faibles du prédicteur soient associées à la valeur `0`, et que les scores élevés du prédicteur soient associées à la valeur `1` de la VD. Nous devons donc "couper" au juste milieu, avec comme but le moins d'erreurs possibles. En d'autre termes, nous voulons le moins de points qui "dépassent" de l'autre côté de notre séparation. Ici, la solution la plus optimale serait la suivante:

<figure markdown>
![Séparation des données (VD binaire)](../graphics/ctree-theory-2.png){ width="450" }
</figure>

Selon cette séparation, si un point possède une valeur du prédicteur supérieure à `9.7`, il a de fortes probabilités de posséder une valeur de la VD de `1` &mdash; et inversement.

Pour des modèles contenant des variables dépendantes numériques, il est nécessaire de procéder à plusieurs séparations:

<figure markdown>
![Séparation des données (VD numérique)](../graphics/ctree-theory-3.png){ width="450" }
</figure>

!!! tip "Remarque"

    L'avantage de procéder de la sorte (plusieurs séparations) est que la linéarité ou non de la distribution des points ne change rien.

## Construction

La construction d'un arbre de décision se fait donc en plusieurs étapes (puisqu'il est nécessaire d'effectuer plusieurs séparations). 

Considérons des données dont nous souhaitons pouvoir prédire le groupe `G`, sur la base de deux prédicteurs `X1` et `X2`:

<figure markdown>
![Données](../graphics/ctree-theory-4.png){ width="450" }
</figure>

Pour construire l'arbre, il est nécessaire de procéder à une première séparation.

<figure markdown>
![](../graphics/ctree-theory-5.png){ width="450" }
</figure>

Il est dès lors possible de créer les deux premières branches de l'arbre de décision:

<figure markdown>
![](../graphics/ctree-theory-6.png){ width="600" }
</figure>

Puis, il faut, pour chaque branche, créer une nouvelle séparation, afin d'isoler les données pour pouvoir prédire le groupe:

<figure markdown>
![](../graphics/ctree-theory-7.png){ width="450" }
</figure>

Ensuite, il est à nouveau possible de créer de nouvelles sous-branches dans l'arbre de décision:

<figure markdown>
![](../graphics/ctree-theory-8.png){ width="600" }
</figure>

Au final, les données découpées ressembleront plus ou moins à cela:

<figure markdown>
![](../graphics/ctree-theory-9.png){ width="450" }
</figure>

Et l'arbre de décision sera tout autant complexe.

!!! warning "Attention à ne pas trop séparer !"

    Il faut tout de fois faire attention à **ne pas trop séparer** (_oversplit_ en anglais) les données. Si les séparations sont trop précises, il peut être difficile de les généraliser sur de nouvelles données, puisqu'elles seront tellement bien adaptées aux données de base, qu'elles seront sensibles aux variations aléatoires et aux erreurs de mesure.

## Mesure de la performance

Il existe différentes mesures de la performance (manières de mesurer à quel point les prédictions d'un algorithme sont bonnes ou non), en fonction du type de variable dépendante:

- Pour les arbres de régression (VD numérique ou continue), on utilise le **MSE** (Mean Squared Error) ou le **RMSE** (Root Mean Squared Error).
- Pour les arbres de classification (VD catégorielle), on utilise des indices tels que l'**exactitude** (accuracy), **l'exactitude balancée** (balanced accuracy), **F1**, la **sensibilité** (sensitivity), la **spécificité** (specificity), etc. Pour calculer ces indices, il est nécessaire d'utiliser des [matrices de confusion](./confusion-matrix.md).

## Ajustement et compromis biais-variance

Comme mentionné précédemment, il est nécessaire de ne pas trop séparer les données (c'est-à-dire de rajouter trop de branches à l'arbre de décision), puisque cela amène l'arbre à être trop spécialisé aux données d'entraînement uniquement. Le modèle peut donc être trop adapté (trop complexe) tout comme il peut ne pas être suffisamment complexe pour les données.

- Lorsque le modèle n'est pas assez complexe, on parle de **sous-ajustement** (_underfit_). Dans ce cas, toutes les prédictions seront incorrectes car le modèle n'est pas assez précis.
- Lorsque le modèle est trop complexe, on parle de **sur-ajustement** (_overfit_). Dans ce cas, le modèle est trop adapté aux données d'entraînement et n'est donc pas généralisable à d'autres données.
- Lorsque le modèle est suffisamment complexe pour les données, mais pas trop, on parle de **bon ajustement** (_good fit_). C'est ce que nous recherchons.

=== "Sous-ajustement"

    <figure markdown>
    ![Sous-ajustement](../graphics/underfit.png)
    </figure>

=== "Sur-ajustement"

    <figure markdown>
    ![Sur-ajustement](../graphics/overfit.png)
    </figure>

=== "Bon ajustement"

    <figure markdown>
    ![Bon ajustement](../graphics/goodfit.png)
    </figure>

Les modèles sous-ajustés sont caractérisés par des biais élevés et une variance faible, tandis que les modèles sur-ajustés sont caractérisés par des biais faibles, et une variance élevée. En rajoutant des branches à l'arbre de décision, on réduit les biais, mais en parallèle on augmente la variance. Cela signifie qu'il faut **trouver le juste milieu entre les deux**.

| Ajustement du modèle | Biais | Variance |
| --- | --- | --- |
| Sous-ajusté (underfit) | Élevé | Faible |
| Sur-ajusté (overfit) | Faible | Élevé |

Des **biais élevés** vont résulter en une prédiction toujours incorrecte, quelque soit les données utilisées. Une **variance élevée** se caractérise par des prédictions incorrectes dans les données de test, en raison de leur caractère aléatoire par rapport au modèle qui est "trop" adapté aux données d'entraînement.
