# Forêt aléatoire (random forest)

!!! abstract "Introduction"

    Une forêt aléatoire est un algorithme d'apprentissage automatique qui consiste à utiliser la [méthode bootstrap](../bootstrap.md) afin de construire une multitude d'arbres de décision qui sont ensuite mis ensemble, dans le but d'augmenter la performance du modèle.

## Introduction théorique

Comme il a été mentionné auparavant, la performance d'un modèle de prédiction dépend de la taille de l'échantillon sur lequel il a été entraîné. Les forêts aléatoires permettent de construire plusieurs arbres de décision différents, grâce à la [méthode bootstrap](../bootstrap.md) qui permet de simuler des données similaires aux données d'origine de manière aléatoire. En combinant tous les arbres réalisés, il est ensuite possible d'obtenir un modèle de prédiction très précis.

## Application

```R title="Syntaxe R"
# Soit y la variable dépendante catégorielle,
# data_training le dataframe contenant les données d'entraînement
# et data_testing le dataframe contenant les données de test
library(randomForest)
forest <- randomForest(y ~ ., data=data_training)
pred <- predict(forest, type="response", newdata=data_testing)
```

Pour visualiser les résultats et obtenir des informations relatives à la performance du modèle, nous utilisons [une matrice de confusion](../ml/confusion-matrix.md):

```R title="Syntaxe R"
# Soit y la variable dépendante,
# pred le vecteur des prédictions de y,
# et data_testing le dataframe contenant les données de test
library(caret)
confusionMatrix(data=pred, reference=data_testing$y)
```

## Exemple

Reprenons les données utilisées pour construire l'arbre de décision que nous avons ajusté précédemment. Nous voulons cette fois créer une forêt aléatoire afin de prédire `Y` de manière plus précise, sur la base de `X1` et `X2`.

| :octicons-file-directory-open-fill-16: Fichiers |
| --- |
| [:fontawesome-solid-file-csv: decision-trees-testing-data.csv](../data/decision-trees-testing-data.csv) |
| [:fontawesome-solid-file-csv: decision-trees-training-data.csv](../data/decision-trees-training-data.csv) |
| [:simple-r: random-forest.R](../data/random-forest.R) |
| [:simple-r: decision-trees-datagen.R](../data/decision-trees-datagen.R) |

Commençons par activer les librairies nécessaires:

```R title="Activation des librairies"
library(party)
library(caret)
library(randomForest)
library(ggplot2)
```

Puis, importons et recodons nos données:

```R title="Importation et recodage des données"
df_training <- read.csv("decision-trees-training-data.csv")
df_testing <- read.csv("decision-trees-testing-data.csv")
df_training$Y <- as.factor(df_training$Y)
df_testing$Y <- as.factor(df_testing$Y)
```

Maintenant, nous pouvons créer une forêt aléatoire:

```R title="Forêt aléatoire"
forest <- randomForest(Y ~ ., data=df_training)
```

Une fois la forêt ajustée, nous pouvons commencer à faire des prédictions avec:

```R title="Prédictions"
pred <- predict(forest, type="response", newdata=df_testing)
```

Ensuite, nous comparons les données prédites avec les données réelles:

```R title="Visualisation de quelques données"
head(pred)
head(df_testing$Y)
```

| **pred** | **df_testing$Y** |
| --- | --- |
| C | C |
| B | B |
| B | B |
| D | D |
| A | B |
| C | C |
| ... | ... |

Nous voyons ici qu'il y a une erreur dans l'une de prédictions.
Ensuite, nous pouvons comparer les deux ensembles de données visuellement:

```R title="Représentation graphique des prédictions et des données de test"
ggplot(df_testing, aes(x=X1, y=X2, colour=pred)) +
  geom_point(cex=2) +
  ggtitle("Prédictions")
ggplot(df_testing, aes(x=X1, y=X2, colour=Y)) +
  geom_point(cex=2) +
  ggtitle("Données de test")
```

=== "Prédictions"

    <figure markdown>
    ![Représentation graphique des prédictions](../graphics/random-forest-example-1.png)
    </figure>

=== "Vraies données"

    <figure markdown>
    ![Représentation graphique des données de test](../graphics/random-forest-example-2.png)
    </figure>

Puis nous pouvons réaliser une matrice de confusion, afin d'évaluer la précision des prédictions faites sur la base de la forêt aléatoire:

```R title="Matrice de confusion"
confusionMatrix(data=pred, reference=df_testing$Y)
```

| | **Ref A** | **Ref B** | **Ref C** | **Ref D** |
| --- | --- | --- | --- | --- |
| **Pred A** | 129 | 11 | 0 | 0 |
| **Pred B** | 24 | 320 | 27 | 0 |
| **Pred C** | 0 | 21 | 288 | 33 |
| **Pred D** | 0 | 0 | 2 | 145 |

| **Overall Statistics** | |
| --- | --- |
| **Accuracy** | 0.882 |
| **95% CI** | (0.8604, 0.9013) |
| **No Information Rate** | 0.352 |
| **P-Value [Acc > NIR]** | < 2.2e-16 |
| **Kappa** | 0.8346 |
| **Mcnemar's Test P-Value** | NA |

| **Statistics by Class** | **A** | **B** | **C** | **D** |
| --- | --- | --- | --- | --- |
| **Sensitivity** | 0.8431 | 0.9091 | 0.9085 | 0.8146 |
| **Specificity** | 0.9870 | 0.9213 | 0.9209 | 0.9976 |
| **Pos Pred Value** | 0.9214 | 0.8625 | 0.8421 | 0.9864 |
| **Neg Pred Value** | 0.9721 | 0.9491 | 0.9559 | 0.9613 |
| **Prevalence** | 0.1530 | 0.3520 | 0.3170 | 0.1780 |
| **Detection Rate** | 0.1290 | 0.3200 | 0.2880 | 0.1450 |
| **Detection Prevalence** | 0.1400 | 0.3710 | 0.3420 | 0.1470 |
| **Balanced Accuracy** | 0.9151 | 0.9152 | 0.9147 | 0.9061 |

Dans l'ensemble, nous voyons que l'algorithme est maintenant plus exact pour prédire `Y`. En effet, lorsque nous utilisions un arbre seul, son exactitude était de `0.833` (95% CI = [0.8084, 0.8556]), alors qu'elle est maintenant de `0.882` (95% CI = [0.8604, 0.9013]).
