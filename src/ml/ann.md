# Réseaux de neurones artificiels (Artificial Neural Networks)

!!! abstract "Introduction"

    Les réseaux de neurones artificiels (Artificial Neural Networks ou _ANN_ en anglais) sont un algorithme d'apprentissage automatique avancé.

## Introduction théorique

Les réseaux de neurones artificiels sont inspirés du fonctionnement du cerveau biologique.

Dans le cerveau, les neurones reçoivent des informations de la part d'autres neurones avec lesquels ils sont connectés. En calculant la somme de ces informations, un neurone détermine s'il doit à son tour envoyer des messages à d'autres neurones.

Dans un réseau artificiel, le processus est plus ou moins le même : un neurone calcule la somme des informations qu'il reçoit, et détermine la sortie sur cette base.

Un réseau artificiel de neurones est composé de plusieurs "couches". Tout à gauche se trouve la **couche d'entrée**, dans laquelle il existe un neurone pour chaque prédicteur. Ensuite, il existe plusieurs **couches cachées**, qui servent à calculer la sortie sur la base des entrées. Enfin, tout à droite, se trouve la **couche de sortie**, dans laquelle il existe un neurone pour chaque niveau possible de la variable dépendante catégorielle. Le résultat de la prédiction correspond au neurone qui est activé le plus fortement dans la couche de sortie.

<figure markdown>
![Un réseau de neurones artificels](../graphics/ann-theory-1.png){ width="500" }
</figure>

Au moment de la prédiction, chaque neurone de la couche d'entrée contient un nombre entre 0 et 1, que l'on appelle son **activation**.
Chaque neurone est ensuite connecté avec tous les neurones de la couche suivante. 

Pour prédire le neurone à activer dans la couche suivante, on somme le produit &mdash; pour chaque neurone &mdash; de son activation et du **poids** de la connexion avec le neurone suivant. On parle ici de **somme pondérée**. Si nous voulons représenter la formule:

<figure markdown>
![sum(w1*a1 + w2*a2 + w3*a3 + ... + wn*an)](../latex/ann-layer.svg)
</figure>

Le poids représente la force (c'est-à-dire l'importance) de la connexion entre deux neurones. Au départ, ces poids sont très faibles et répartis aléatoirement. Plus le modèle se spécialise, plus ces poids deviennent précis.

<figure markdown>
![Poids dans un réseau de neurones artificels](../graphics/ann-theory-2.png){ width="500" }
</figure>

Lorsqu'une erreur de prédiction est détectée, le modèle calcule d'abord le _coût_ de celle-ci, soit:

<figure markdown>
![cost = sum( (prediction - reality)^2 )](../latex/cost.svg)
</figure>

Comme pour la régression, le but est de minimiser les erreurs. Les poids sont alors ajustés dans par **rétropropagation** (c'est-à-dire que l'on commence par les couches les plus à droite pour aller de plus en plus à gauche).

## Avantages et inconvénients de cet algorithme

Par rapport aux arbres de décision et aux forêts aléatoires, cet algorithme a l'avantage de pouvoir traiter des données bien plus complexes, multimodales, etc.

!!! example "Exemple"

    Il peut détecter un chiffre qui est écrit dans une image, en utilisant **chaque pixel** de l'image comme un prédicteur. Pour une image de 28 pixels sur 28 pixels, il y a donc 784 prédicteurs.

Cependant, les ANN ont besoin d'être entraînés sur une énorme quantité de données pour pouvoir fonctionner.

## Application

```R title="Syntaxe R"
# Soit y la variable dépendante catégorielle,
# x1, x3, x3 et x4 les prédicteurs
# data_training le dataframe contenant les données d'entraînement
# et data_testing le dataframe contenant les données de test
library(neuralnet)
ann <- neuralnet(
  y ~ x1 + x2 + x3 + x4,
  data=data_training,
  hidden=c(2,4,2),
  linear.output=FALSE
)
results <- compute(
    ann, 
    data_testing[,c("x1", "x2", "x3", "x4")]
)
pred_cls <- apply(results$net.result, 1, which.max)
pred <- levels(data_testing$y)[pred_cls]
accuracy <- sum(data_testing$y == pred) / length(pred)
print(paste("Accuracy:", accuracy))
```

## Exemple


Voici nos données et notre fichier d'analyse:

| :octicons-file-directory-open-fill-16: Fichiers |
| --- |
| [:fontawesome-solid-file-csv: ann.csv](../data/ann.csv) |
| [:simple-r: ann.R](../data/ann.R) |

Commençons par activer les librairies dont nous avons besoin:

```R title="Activation des librairies"
library(neuralnet)
library(caret)
```

Puis importons nos données:

```R title="Importation et recodage des données"
df <- read.csv("ann.csv")
df$y <- as.factor(df$y)
```

Procédons maintenant à une [séparation des données](../cross-validation/introduction.md) (dans la réalité, il vaudrait mieux utiliser une technique de [validation croisée](../cross-validation/k-fold.md) à la place). Ici, nous voulons utiliser 80% de nos données pour entraîner le modèle, et 20% pour le tester:

```R title="Séparation des données"
parts <- createDataPartition(y=df$y, p=.8, list=F)
df_training <- df[parts,]
df_testing <- df[-parts,]
```

Construisons ensuite le réseau de neurones artificiels et entraînons le:

```R title="Réseau du neurones artificiels"
nn <- neuralnet(
  y ~ x1+x2+x3+x4,
  data=df_training,
  hidden=c(2, 4, 2),
  linear.output=FALSE
)
```

Nous pouvons maintenant visualiser notre réseau:

```R title="Visualisation du réseau"
plot(nn)
```

<figure markdown>
![#](../graphics/ann-example-1.png)
</figure mardown>

Testons maintenant le modèle avec nos données de test:

```R title="Prédictions"
results <- compute(
  nn, 
  df_testing[c("x1", "x2", "x3", "x4")]
)
pred_cls <- apply(results$net.result, 1, which.max)
pred <- levels(df_testing$y)[pred_cls]
```

Nous pouvons finalement calculer l'exactitude du modèle:

```R title="Calcul de l'exactitude"
accuracy <- sum(df_testing$y == pred) / length(pred)
print(paste("Accuracy:", accuracy))
```

| Accuracy |
| --- |
| 1 |

L'exactitude étant de `1`, cela signifie qu'il n'y a eu aucune erreur lors du test. Notre modèle est donc **très bon**.
