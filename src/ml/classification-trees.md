# Arbres de classification (classification trees)

!!! abstract "Introduction"

    Les arbres de classification sont des [arbres de décision](./decision-trees.md) utilisés pour prédire une variable dépendante catégorielle.

## Application

Avec R, il est possible de créer un arbre de décision et de prédire des scores sur la base de l'arbre de la manière suivante :

```R title="Syntaxe R"
# Soit y la variable dépendante catégorielle,
# data_training le dataframe contenant les données d'entraînement
# et data_testing le dataframe contenant les données de test
library(party)
tree <- ctree(y ~ ., data=data_training)
plot(tree)  # pour visualiser l'arbre
pred <- predict(tree, type="response", newdata=data_testing)
```

!!! info "Information"

    Ici, le point `.` qui est spécifié à la place des prédicteurs est une valeur qui permet de dire à R que l'on souhaite inclure toutes les autres variables comprises dans le dataframe, à l'exception de la variable dépendante.

    Avec un dataframe dont les colonnes sont `y`, `x1`, `x2` et `x3`, l'expression `y ~ .` est équivalente à `y ~ x1+x2+x3`.

Il est ensuite possible de vérifier les prédictions afin d'identifier les erreurs, et de calculer des indices relatifs à la performance du modèle, à l'aide d'une [matrice de confusion](../ml/confusion-matrix.md):

```R title="Syntaxe R"
# Soit y la variable dépendante,
# pred le vecteur des prédictions de y,
# et data_testing le dataframe contenant les données de test
library(caret)
confusionMatrix(data=pred, reference=data_testing$y)
```

## Exemple

Voici nos données et notre fichier d'analyse &mdash; ainsi que le script R permettant de générer les fichiers de données.

| :octicons-file-directory-open-fill-16: Fichiers |
| --- |
| [:fontawesome-solid-file-csv: decision-trees-testing-data.csv](../data/decision-trees-testing-data.csv) |
| [:fontawesome-solid-file-csv: decision-trees-training-data.csv](../data/decision-trees-training-data.csv) |
| [:simple-r: decision-trees.R](../data/decision-trees.R) |
| [:simple-r: decision-trees-datagen.R](../data/decision-trees-datagen.R) |

Ici, nous souhaitons réussir à prédire la variable dépendante `Y`, sur la base de deux prédicteurs: `X1` et `X2`.

Commençons par activer les librairies nécessaires:

```R title="Activation des librairies"
library(party)
library(caret)
library(ggplot2)
```

Ensuite, nous devons importer nos données d'entraînement et de test:

```R title="Importation et recodage des données"
df_training <- read.csv("decision-trees-training-data.csv")
df_testing <- read.csv("decision-trees-testing-data.csv")
df_training$Y <- as.factor(df_training$Y)
df_testing$Y <- as.factor(df_testing$Y)
```

Nous pouvons visualiser nos données d'entraînement graphiquement:

```R title="Représentation graphique des données d'entraînement"
ggplot(df_training, aes(x=X1, y=X2, colour=Y)) +
  geom_point(cex=2) +
  ggtitle("Données d'entraînement")
```

<figure markdown>
![Représentation graphique des données d'entraînement](../graphics/ctree-example-1.png){ width="450" }
</figure>

Nous pouvons ensuite créer l'arbre de décision:

```R title="Arbre de décision"
tree <- ctree(Y ~ ., data=df_training)
```

Que nous pouvons visualiser de la manière suivante:

```R title="Visualisation de l'arbre de décision"
plot(tree)
```

<figure markdown>
![Représentation graphique des données d'entraînement](../graphics/ctree-example-2.png)
</figure>

Maintenant que l'arbre de décision existe, nous pouvons commencer à faire des prédictions avec. 
À noter que nous souhaitons prédire des données qui sont différentes de celles avec lesquelles nous avons entraîné l'algorithme. En effet, si nous réutilisons les mêmes données, tout ce processus a peu d'intérêt : nous ne souhaitons pas prédire ce que nous connaissons déjà.

Nous calculons donc les prédictions pour nos données de test &mdash; et non plus les données d'entraînement:

```R title="Prédictions"
pred <- predict(tree, type="response", newdata=df_testing)
```

Nous pouvons maintenant comparer nos prédictions avec la réelle valeur de la variable dépendante dans nos données:

```R title="Visualisation de quelques données"
head(pred)
head(df_testing$Y)
```

| **pred** | **df_testing$Y** |
| --- | --- |
| C | C |
| B | B |
| B | B |
| D | D |
| B | B |
| C | C |
| ... | ... |

Les premières prédictions semblent donc correctes.

Nous pouvons également représenter graphiquement nos données de test, ainsi que nos prédictions, afin d'avoir une vision globale:

```R title="Représentation graphique des prédictions et des données de test"
ggplot(df_testing, aes(x=X1, y=X2, colour=pred)) +
  geom_point(cex=2) +
  ggtitle("Prédictions")
ggplot(df_testing, aes(x=X1, y=X2, colour=Y)) +
  geom_point(cex=2) +
  ggtitle("Données de test")
```

=== "Prédictions"

    <figure markdown>
    ![Représentation graphique des prédictions](../graphics/ctree-example-3.png)
    </figure>

=== "Vraies données"

    <figure markdown>
    ![Représentation graphique des données de test](../graphics/ctree-example-4.png)
    </figure>

Nous pouvons finalement vérifier le nombre d'erreur, ainsi que le type d'erreur qui se sont produites à l'aide d'une matrice de confusion:

```R title="Matrice de confusion"
confusionMatrix(data=pred, reference=df_testing$Y)
```

| | **Ref A** | **Ref B** | **Ref C** | **Ref D** |
| --- | --- | --- | --- | --- |
| **Pred A** | 127 | 20 | 0 | 0 |
| **Pred B** | 26 | 287 | 32 | 0 |
| **Pred C** | 0 | 45 | 281 | 40 |
| **Pred D** | 0 | 0 | 4 | 138 |

| **Overall Statistics** | |
| --- | --- |
| **Accuracy** | 0.833 |
| **95% CI** | (0.8084, 0.8556) |
| **No Information Rate** | 0.352 |
| **P-Value [Acc > NIR]** | < 2.2e-16 |
| **Kappa** | 0.7664 |
| **Mcnemar's Test P-Value** | NA |

| **Statistics by Class** | **A** | **B** | **C** | **D** |
| --- | --- | --- | --- | --- |
| **Sensitivity** | 0.8301 | 0.8153 | 0.8864 | 0.7753 |
| **Specificity** | 0.9764 | 0.9105 | 0.8755 | 0.9951 |
| **Pos Pred Value** | 0.8639 | 0.8319 | 0.7678 | 0.9718 |
| **Neg Pred Value** | 0.9695 | 0.9008 | 0.9432 | 0.9534 |
| **Prevalence** | 0.1530 | 0.3520 | 0.3170 | 0.1780 |
| **Detection Rate** | 0.1270 | 0.2870 | 0.2810 | 0.1380 |
| **Detection Prevalence** | 0.1470 | 0.3450 | 0.3660 | 0.1420 |
| **Balanced Accuracy** | 0.9032 | 0.8629 | 0.8810 | 0.8852 |

Dans l'ensemble, notre arbre permet donc de prédire `Y` avec une exactitude de `0.833` (95% CI = [0.8084, 0.8556]). Il est particulièrement bon pour prédire le groupe `A`, avec une exactitude balancée de `0.9032`.
