# Théories formalisées

## Paradigme de la recherche

Le paradigme de la recherche est le suivant :

1. Tout d'abord, nous avons une **théorie**.
1. Celle-ci conduit à des **hypothèses**.
1. Ces hypothèses influencent le choix du **design** de l'étude (transversal, longitudinal, en laboratoire, etc).
1. Ensuite vient le choix de la **mesure**.
1. Puis la **collecte de données**.
1. Une fois les données collectées, vient **l'analyse** de celles-ci.
1. Lorsque l'analyse est réalisée, vient la dernière étape, à savoir **l'interprétation des résultats**.

Les théories, dont le but est d'expliquer les processus causaux dans un phénomène, doivent donc être testables. Pour cela, il faut que leurs limites soient claires, et qu'elles soient suffisamment simples pour être testées (il ne faut toutefois pas qu'elles soient trop simples, au risque de devenir inutiles).

## Comparaison avec l'approche traditionnelle

En statistiques, de nombreux modèles existent : la régression linéaire, la régression multiple, la modération, etc. Il est donc compliqué de savoir quel modèle utiliser dans l'approche traditionnelle &mdash; c'est-à-dire sans formalisation de la théorie. En effet, si une variable est une fourchette, il est nécessaire de l'inclure dans la régression multiple. Mais s'il s'agit d'un collisionneur, l'inclure ne fera que créer un problème. 

C'est pour cette raison qu'il est nécessaire de **formaliser la théorie** avec un DAG. Si le DAG est élaboré correctement, alors un seul modèle est possible à utiliser. L'usage d'un DAG permet de déterminer quel modèle utiliser, mais également le design de recherche. Cela permet également de parler de causalité, là où la situation n’est pas très claire dans l’approche traditionnelle.
