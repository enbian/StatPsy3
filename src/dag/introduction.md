# Introduction aux DAG

!!! abstract "Introduction"
    Un DAG (_Directed Acyclical Graph_) est une manière de représenter (à l'aide d'un diagramme) un modèle dans lequel sont présents des liens causaux entre plusieurs variables.

## Introduction

Les DAG permettent de formaliser des théories (dont le but est d'expliquer des processus causaux). Par la formalisation d'une théorie, il est possible de dépasser l'approche traditionnelle en statistiques et de tester des liens causaux. À noter que la causalité est **probabiliste en psychologie**.

!!! question "Types de causalité"

    La causalité peut être déterministe ou probabiliste:

    - Dans une approche causale déterministe `X` cause `Y`, si `X` garantit toujours `Y`.
    - Dans une approche probabliste, `X` cause `Y` si `X` augmente la probabilité de `Y`.


## Terminologie

Le terme _Directed_ (dirigé) signifie que la relation entre deux variables doit être causale et donc unidirectionnelle. Le terme _Acyclical_ (acyclique), quant à lui, signifie que le modèle ne doit pas s'auto-influencer. Si la relation forme une boucle (par exemple `A` influence `B`, `B` influence `C` et `C` influence `A`), alors on ne peut pas réaliser de DAG.

Dans un DAG, les relations entre les variables sont représentées par des flèches, et les variables elles-mêmes sont représentées par des "noeuds".

* Une variable `X` qui influence directement une variable `Z` est appelée son **parent**.
* Une variable `Z` qui est influencée directement par `X` est appelée son **enfant**.
* Une variable `Y` qui est influencée indirectement ou directement par `X` est appelée son **descendant**.
* Une variable `X` qui influence directement ou indirectement `Y` est appelée son **ancêtre**.

## Jonctions

Une jonction est l'ensemble des relations entre trois variables données dans un DAG. Il existe trois types de jonctions différentes qui peuvent être présentes dans ce type de diagramme:

| Image | Nom | Syntaxe R | Description |
| --- | --- | --- | --- |
| ![X -> Z -> Y](../graphics/dag-chain.png) | **Chaîne** (ou médiation) | `Y ~ X` | Il y a une relation causale entre `X` et `Y`, car toutes les flèches vont dans la même direction.<br><br>Il ne faut pas inclure `Z` dans le modèle. En effet, puisque `Z` est entièrement influencé par `X`, le fait de l'inclure comme un deuxième prédicteur reviendrait à bloquer l'effet de `X` sur `Y`. |
| ![X <- Z -> Y](../graphics/dag-fork.png) | **Fourchette** | `Y ~ X+Z` | Il n'y a pas de relation causale entre `X` et `Y`, car les flèches ne vont pas toutes dans le même sens. Il y a cependant une dépendance entre les deux, puisque `X` et `Y` corrèlent.<br><br>Il est nécessaire d'inclure `Z` dans le modèle. |
| ![X -> Z <- Y](../graphics/dag-collider.png) | **Collisionneur** | `Y ~ X` | Il n'y a pas de dépendance, ni de chemin statistique entre `X` et `Y`, donc il n’y a pas de relation causale (ni non-causale) entre les deux.<br><br>Étant donné qu'il n'existe pas de relation statistique, il n'est pas possible d'inclure `Z` dans le modèle. Autrement, cela reviendrait à créer une relation statistique entre `X` et `Y`. |

## Calculer les effets

Pour calculer les effets d'un prédicteur sur la variable dépendante, il suffit de multiplier les effets du prédicteur avec ceux des variables modératrices.

Prenons le modèle suivant:

<figure markdown>
![X -> Z -> Y](../graphics/dag-chain.png){ width="300" }
Ici, `X` a un effet de `0.4` sur `Z`, et `Z` a un effet de `0.3` sur `Y`.
</figure>

Dans ce modèle, l'effet de `X` sur `Y` est donc le produit de l'effet de `X` sur `Z` et de l'effet de `Z` sur `Y`. Ici, le résultat est donc: `0.4 * 0.3 = 0.12`. Cependant, si nous cherchons l'effet de `Z` sur `Y` uniquement, alors `X` n'a plus aucune importance dans le modèle. L'effet que nous recherchons est donc celui de `Z` sur `Y`, soit `0.3`.

Si maintenant nous souhaitons calculer à la fois des effets indirects et directs, il nous faut additionner les produits entre eux.

<figure markdown>
![X -> Z -> Y et X -> Y](../graphics/dag-effect-example.png){ width="300" }
Ici, `X` a un effet de `0.4` sur `Z`, `Z` a un effet de `0.3` sur `Y`, et `X` a un effet direct de `0.2` sur `Y`.
</figure>

Alors l'effet total de `X` sur `Y` est la somme de des effets indirects et directs. Dans ce modèle: `(0.4 * 0.3) + 0.2 = 0.32`.
