# Indépendance conditionnelle

## Symboles de notation

Différents symboles sont utilisés pour indiquer les liens entre les variables en notation scientifique.

- **L'indépendance** entre deux variables est représentée par le symbole `⫫`.
- La **prise en compte d'une variable dans le modèle** est représentée par le symbole `|`, avec à droite la variable que l'on contrôle.

## Pour les fourchettes

Dans le cas d'une fourchette, les deux enfants de la variable qui est le parent corrèlent si l'on observe leur variance commune.

<figure markdown>
![X <- Z -> Y](../graphics/dag-fork.png){ width="250" }
</figure>

Si l'on considère une maladie `Z` qui cause les symptômes `X` et `Y`, les deux symptômes seront corrélés (cela signifie qu'il existe une dépendance statistique entre les deux). 
Cependant, il est nécessaire de prendre en compte leur cause commune. Si l'on fait cela, alors en principe `X` et `Y` ne devraient plus corréler, puisque les deux sont des causes de `Z`. En effet, la seule raison pour laquelle les deux sont corrélés dans la vie réelle est qu'ils sont causés par la même variable (dans ce cas la maladie `Z`).

Nous pouvons dès lors affirmer que:

- `X ⫫ Y | Z` (`X` et `Y` sont indépendants lorsque l'on contrôle `Z`).

!!! success "Contrôler pour le parent dans la fourchette"
    Lorsque nous avons une fourchette, nous devons inclure le parent dans le modèle de régression. En effet, ne pas le faire engendre une dépendance statistique non-causale (une corrélation) entre les deux enfants. Ce phénomène est connu sous le nom de **backdoor path** (porte dérobée).

## Pour les chaînes

Pour les chaînes, la situation est la même que pour les fourchettes en termes d'indépendance de la cause et de l'effet.

<figure markdown>
![X -> Z -> Y](../graphics/dag-chain.png){ width="250" }
</figure>

Si l'on considère une maladie `X` qui engendre un symptôme `Z`, qui a son tour cause la prise d'un médicament `Y`, on comprend aisément que `X` et `Y` sont associés dans la vraie vie. Cependant, il est impossible de comprendre cette dépendance sans prendre en compte `Z`. Dès lors que `Z` est contrôlé, alors nous pouvons affirmer statistiquement que:

- `X ⫫ Y | Z` (`X` et `Y` sont indépendants lorsque l'on contrôle `Z`). En d'autres termes, `Z` "bloque" l'effet de `X` sur `Y`.

!!! failure "Ne pas contrôler le médiateur dans une chaîne"
    Lorsque des chaînes sont présentes, il ne faut en principe pas inclure la variable médiatrice dans le modèle, puisque celle-ci "bloque" l'effet de `X` sur `Y`. 

    Similairement, si l'on souhaite connaître l'effet de `Z` sur `Y`, il ne faut pas inclure leur descendant `X`, puisque nous ne nous intéressons pas à ce qui cause `Z`, mais uniquement à ce que `Z` engendre sur `Y`. 
    
    Il en va de même si l'on s'intéresse à l'effet de `X` sur `Z` : dans ce cas, il ne faut pas inclure `Y` dans le modèle. En effet, à ce moment, l'effet de `Z` sur `Y` n'est pas important : on ne s'intéresse qu'à l'effet de `X` sur `Z`.

## Pour les collisionneurs

Pour les collisionneurs, la situation est tout à fait différente.

<figure markdown>
![X -> Z <- Y](../graphics/dag-collider.png){ width="250" }
</figure>

En effet, si l'on considère une maladie `X` et une autre maladie `Y`, alors les deux sont indépendantes. Cependant, si l'on inclut le symptôme `Z` (la fièvre), alors `X` et `Y` deviennent dépendantes l'une de l'autre, puisqu'elles sont associées à leur effet commun `Z`. Dès lors que leur effet commun est pris en compte, les deux variables parentes sont donc associées de manière non-causale.

Nous pouvons donc affirmer que:

- `X ⫫ Y` (`X` et `Y` sont indépendants &mdash; pour autant que l'on ne contrôle pas `Z`).

!!! failure "Ne pas contrôler le descendant commun dans un collisionneur"
    Lorsqu’un collisionneur est présent, il ne faut pas inclure celui-ci dans le modèle. En effet, en l'incluant dans le modèle, on associe de manière non-causale deux variables qui sont indépendantes l'une de l'autre.
