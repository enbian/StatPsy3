# Simulation de données

## Génération des données aléatoires

Il est possible de simuler/générer des données avec R.

Ici, nous allons générer des données pour deux variables: `X` et `Y`. Nous voulons que `X` ait une influence sur `Y`.

!!! info "Avant de commencer"

    Avant de commencer, nous pouvons régler R pour lui dire quelle source aléatoire (appelée _graine_ en informatique) nous souhaitons utiliser pour la génération aléatoires des données.

    C'est un peu compliqué, mais ce qu'il faut retenir est que cela va permettre à n'importe quelle personne qui essaie de répliquer l'exemple sur cette page d'obtenir exactement les mêmes valeurs.
    
    Pour prendre un exemple, si nous demandons à une personne de se promener en tournant deux fois à droite, une fois à gauche, puis de répéter la séquence, en se promenant dans une ville, nous obtiendrons un itinéaire différent en fonction de la ville et de l'endroit où la personne est placée au début. Cependant, si nous plaçons la personne toujours au même endroit, dans la même ville, alors l'itinéraire sera le même à chaque fois.
    C'est en quelque sorte ce que nous souhaitons faire ici: nous indiquons simplement à R _de quel endroit partir_ pour générer des données aléatoires.

    ```R title="Définir la 'graine' aléatoire"
    set.seed(100)
    ```

Tout d'abord, nous devons spécifier la taille de notre échantillon:

```R title="Taille de l'échantillon"
N <- 1000
```

Ensuite, nous devons définir la source de variation de `X`. Dans notre cas (où `X -> Y`), `X` n'est pas causée par une variable clairement définie. La valeur de `X` dépend donc d'un processus aléatoire que nous allons appeler `Ux`. Ici, nous utilisons la fonction `rnorm` (qui permet de générer des données aléatoires distribuées normalement):

```R title="Processus aléatoire à l'origine des variations de X"
Ux <- rnorm(N)
```

Ensuite, nous devons définir les valeurs de `X`. Puisqu'ici, `X` n'est incluencée que par le processus aléatoire `Ux` et pas par des autres variables, `X` possède donc les mêmes valeurs que `Ux`. Nous pouvons donc écrire:

```R title="Valeurs de X"
X <- Ux
```

Ensuite, nous devons nous occuper de `Y`. Dans notre modèle, `Y` est influencé par `X`. Cependant, dans le monde réel, il y a toujours une part de hasard qui a également une influence (par exemple une erreur de mesure, une variable tierce non identifiée, l'heure de la journée, des biais, etc). Ici, nous allons donc définir cette part de hasard comme un processus aléatoire `Uy`.

Dans notre modèle, `Y` est donc influencé à la fois par `X` et par `Uy`.

Nous pouvons donc définir maintenant le processus aléatoire `Uy`:

```R title="Processus aléatoire à l'origine d'une partie des variations de Y"
Uy <- rnorm(N)
```

Puis, nous pouvons définir les valeurs de `Y`. Cependant, il nous reste encore à définir la force de l'influence de `X`, sur `Y`. Ici, nous souhaitons que `Y` soit influencé par `X` à manière de `0.4` unités de `Y` par unité de `X`.
Nous pouvons donc définir la relation entre `X` et `Y` comme une pente `B`:

```R title="Force de la relation entre X et Y"
B <- 0.4
```

Finalement, nous pouvons donc générer les valeurs de `Y`, en prenant en compte les informations précédemment générées:

```R title="Valeurs de Y"
Y <- B*X + Uy
```

## Vérification des données générées

Nous pouvons maintenant tester nos données, afin de voir si celles-ci respectent les conditions que nous avions précédemment définies. Nous souhaitons voir si `X` influence bien les variations de `Y` en les faisant augmenter en moyenne de `0.4` unités par unité de `X`. Pour ce faire, nous devons calculer une régression (linéaire simple, puisque nous n'avons que deux variables).

Pour plus d'informations sur la régression linéaire simple, il est possible de consulter [mes notes de statistiques II](https://stat2.enbian.space/regressions/simple-linear-regression/).

```R title="Régression linéaire simple"
summary(lm(Y ~ X))
```

| | Estimate | Std. Error | t value | p |
| --- | --- | --- | --- | --- |
| **(Intercept)** | 0.003921 | 0.031039 | 0.126 | 0.899 |
| **X** | 0.413415 | 0.030129 | 13.722 | <2e-16 \*\*\* |

Ici, nous pouvons voir que nos données sont presque parfaites. En effet, `X` a une influence sur `Y` (_B_ = 0.413, _p_<.001). Les données générées sont très proches de ce qui nous souhaitions obtenir, puisque le coefficient `B` est égal à `0.41`, et que nous souhaitions obtenir `0.4`.

## Exemple: pour des modèles plus compliqués

Dans la réalité, les modèles sont souvent plus compliqués que le cas présenté ci-dessus. Ici, nous allons générer des données pour le modèle suivant, en souhaitant tester l'influence de `A` sur `D`:

<figure markdown>
![Modèle d'exemple](../graphics/dag-simulation-example.png)
</figure>

Commençons par nous occuper de `A`:

```R title="Génération aléatoire de A"
set.seed(20)
N <- 5000
Ua <- rnorm(N)
A <- Ua
```

Puis `C`:

```R title="Génération aléatoire de C"
Uc <- rnorm(N)
C <- Uc
```

Ensuite, `B`:

```R title="Génération aléatoire de B"
Ub <- rnorm(N)
B <- Ub + 0.4*A + 0.6*C
```

Et enfin, `D`:

```R title="Génération aléatoire de D"
Ud <- rnorm(N)
D <- Ud + 0.5*B + 0.3*C
```

Nous pouvons maintenant réaliser notre modèle de régression. Cependant, nous devons respecter les [règles vues précédemment](./rules.md). Dans ce DAG, nous pouvons affirmer que:

- `A ⫫ C` (`A` est indépendant de `C`, pour autant que l'on ne contrôle pas `B` dans le modèle).
- `A ⫫ D | (B,C)` (`A` est indépendant de `D` si l'on contrôle `B` et `C` dans le modèle). Cela signifie qu'inclure `B` et `C` bloque l'effet de `A` sur `D`.

Puisque `A -> B -> D` est une chaîne, il n'est pas judicieux d'inclure `B` dans notre modèle de régression. En effet, nous avons vu précédemment que l'inclusion de la variable modératrice d'une chaîne bloque l'effet de son parent sur son descendant. De plus, si nous avions inclus `B`, nous aurions dû également inclure `C`. En effet, le bloc `B <- C -> D` est une fourchette, et ne pas l'inclure reviendrait laisser ouverte une _porte dérobée_.

Nous pouvons maintenant écrire:

```R title="Modèle de régression"
m1 <- lm(D ~ A)
summary(m1)
```

| | Estimate | Std. Error | t value | p |
| --- | --- | --- | --- | --- |
| **(Intercept)** | 0.01347 | 0.01797 | 0.75 | 0.453 |
| **A** | 0.21010 | 0.01802 | 11.66 | <2e-16 \*\*\* |

Ici, le coefficient de régression a une valeur de `0.21`, ce qui est très proche de ce que nous souhaitions obtenir. En effet, si nous calculons l'effet de `A` sur `D`, nous devons multiplier l'effet de `A` sur `C` avec l'effet de `C` sur `D`. Dans ce cas, nous obtenons: `(0.4 * 0.5) = 0.2`.

Si maintenant nous souhaitons, toujours avec le même DAG, calculer l'effet de `B` sur `D`, nous devons:

- Ignorer `A`, qui n'est pas important pour ce nouveau modèle.
- Inclure `C` dans le modèle, afin de fermer la _porte dérobée_ que cette variable engendre (comme il a été mentionné précédemment).

Notre nouveau modèle peut donc être réalisé:

```R title="Modèle de régression"
m2 <- lm(D ~ B + C)
summary(m2)
```

| | Estimate | Std. Error | t value | p |
| --- | --- | --- | --- | --- |
| **(Intercept)** | 0.01268 | 0.01421 | 0.892 | 0.372 |
| **B** | 0.50714 | 0.01308 | 38.776 | <2e-16 \*\*\* |
| **C** | 0.28460 | 0.01647 | 17.282 | <2e-16 \*\*\* |

Ici, nous constatons que `B` a bel et bien un effet sur `D` (_B_ = 0.507, _p_<.001) et que `C` a également un effet sur `D` (_B_ = 0.285, _p_<.001). À nouveau, ces résultats sont très proches de ce que nous attendions puisque nous avons généré un effet de `B` sur `D` d'une valeur `0.5`, et un effet de `C` sur `D` d'une valeur de `0.3`.
