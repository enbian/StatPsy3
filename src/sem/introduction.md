# Introduction aux modèles d'équation structurelle

!!! abstract "Introduction"
    Les modèles d'équations structurelles (SEM) sont similaires aux modèles linéaires vu précédemment, à la différence que ceux-ci peuvent inclure des variables latentes, qu'ils peuvent contenir des liens causaux et que les erreurs de mesure peuvent être prises en compte.

## Utilité des modèles d'équations structurelles (SEM)

Les SEM sont particulièrement utiles pour vérifier si le modèle théorique correspond aux données. Cela permet de rejeter, de modifier ou d'accepter le modèle.

De plus, l'avantage des SEM est qu'ils permettent d'obtenir la valeur de tous les coefficients en même temps, sans avoir besoin de décider quelles variables inclure ou non.

Il est également possible de tester des relations non linéaires avec cette approche.

## Modélisation avec R

La librairie `lavaan` (pour **la**tent **va**riable **an**alysis) permet de réaliser une modélisation d'équations structurelles dans un environnement R.

La syntaxe est la suivante:

```R title="Modélisation d'équation structurelle"
# Soit Z une fourchette dont les descendants sont X et Y,
# et Y un collisionneur dont les parents sont X et Z
library(lavaan)
sem_mod <- 'Y ~ X + Z
  X ~ Z'
m <- sem(sem_mod, data=data)
summary(m)
```

## Interprétation

La syntaxe ci-dessus nous donne plusieurs informations différentes. Certaines d'entre elles sont des indices d'ajustement (_fit indices_ en anglais), qui permettent de vérifier si le modèle dans son ensemble correspond aux données.

La p-valeur calculée sur la base d'une statistique du khi carré est l'une d'entre elles. Dans ce cas, les suppositions sont les suivantes:

| **p < 0.05 \*** | **p >= 0.05** |
| --- | --- |
| Il existe un écart significatif entre les données et le modèle. | L'écart entre les données et le modèle n'est pas statistiquement significatif. |
| Le modèle **ne correspond pas aux données**. | Le modèle semble **correspondre aux données**. |

!!! question "Si la p-valeur n'est pas indiquée?"
    Si la p-valeur n'est pas indiquée et que le khi-carré est égal à zéro, cela indique que l'on a affaire à un modèle **saturé** &mdash; c'est-à-dire un modèle qui correspond **parfaitement** à nos données.

Il existe également d'autres indices d'ajustement, qui seront évoqués plus tard.

## Analyse du chemin

Avec les modèles SEM, il est possible, comme pour les régressions, de déterminer si un **certain chemin statistique entre deux variables est significatif**. Cette méthode d'analyse est appelée **analyse du chemin** (_path analysis_). Ici, l'interprétation est la même que pour les régressions.

## Exemple

Si nous reprenons les données générées précédemment dans l'exemple du chapitre sur les DAG:

| :octicons-file-directory-open-fill-16: Fichiers |
| --- |
| [:fontawesome-solid-file-csv: dag-data-simulation.csv](../data/dag-data-simulation.csv) |

Nous commençons tout d'abord par activer les librairies nécessaires:

```R title="Activation des librairies"
library(lavaan)
```

Puis nous importons les données que nous avions générées dans le chapitre précédent:

```R title="Importation des données"
df <- read.csv("dag-data-simulation.csv")
```

Nous écrivons maintenant notre équation structurelle:

```R title="Équation structurelle"
sem_mod <- 'D ~ C + bd*B
  B ~ C + ab*A
  total := ab*bd'
```

!!! info "Pour information"
    Ici, `bd` est le nom que nous donnons à l'effet de `B` sur `D`, de même que `ab` est le nom que nous donnons à la relation entre `A` et `B`. Ces instructions permettent à `lavaan` de calculer les effets indirects que nous déclarons dans la dernière ligne du modèle. Cela sera abordé plus en détails [dans le chapitre suivant](./mediation.md).

Nous pouvons maintenant réaliser notre modèle sur la base de nos données:

```R title="Analyse du chemin"
m <- sem(sem_mod, data=df)
```

Nous pouvons ensuite réaliser une visualisation graphique du modèle:

```R title="Représentation graphique du modèle"
semPaths(
  m, "std", "est",
  curvePivot=T,
  style="lisrel",
  title=T,
  intercepts=F
)
title("Modèle d'équation structurelle", adj=.25)
```

<figure markdown>
![Représentation graphique de notre modèle](../graphics/sem-simple.png)
</figure>

Puis nous interprétons les résultats:

```R title="Affichage des résultats"
summary(m)
```

| **Model Test User Model** | |
| --- | --- |
| **Test statistic** | 0.029 |
| **Degrees of freedom** | 1 |
| **P-value (Chi-square)** | 0.864 |

| **Regressions** | **Estimate** | **Std.Err** | **z-value** | **P(>\|z\|)** |
| --- | --- | --- | --- | --- |
| **D ~ C** | 0.285 | 0.016 | 17.287 | 0.000 |
| **D ~ B (bd)** | 0.507 | 0.013 | 38.788 | 0.000 |
| **B ~ C** | 0.608 | 0.014 | 42.020 | 0.000 |
| **B ~ A (ab)** | 0.402 | 0.014 | 28.024 | 0.000 |

| **Defined Parameters** | **Estimate** | **Std.Err** | **z-value** | **P(>\|z\|)** |
| --- | --- | --- | --- | --- |
| **total** | 0.204 | 0.009 | 22.715 | 0.000 |


Dans un premier temps, nous constatons que le modèle semble correspondre à nos données (χ^2^~(1)~ = 0.03, _p_ = .864)

Ensuite, nous pouvons voir que les résultats semblent corrects. En effet, `B` a un effet de `0.507` sur `D`, ce qui est le même résultat qu'une régression linéaire pour laquelle on contrôlerait `C`. De plus, l'effet indirect de `A` sur `D` est de `0.204`, ce qui correspond à l'effet attendu de `A` sur `D` sans contrôler `B` &mdash; qui devrait être d'environ `0.2`.

De plus, tous les résultats sont significatifs (_p_<.001), ce qui suggère que le modèle est adapté aux données empiriques.
