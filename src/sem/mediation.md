# Modèles de médiation

!!! abstract "Introduction"
    Les modèles de médiation permettent de mesurer l'effet indirect d'un prédicteur sur la variable dépendante, par l'inclusion d'une troisième variable dans le modèle. Cette variable est appelée une **variable médiatrice**.

!!! warning "Attention"
    Il existe une différence fondamentale entre la _modération_ et la _médiation_. En effet, alors que [la modération](https://stat2.enbian.space/regressions/moderation/) permet de calculer **l'impact d'une troisième variable sur la relation entre un prédicteur et une VD**, la médiation permet de mesurer **l'effet indirect d'un prédicteur sur la VD**, via la variable médiatrice.

## Introduction théorique

Dans un modèle de médiation, l'intérêt est de mesurer l'effet indirect du prédicteur (c'est-à-dire l'effet qui passe "à travers" la médiatrice) sur la variable dépendante.

Ici, nous ne nous intéressons donc plus à `X -> Y`, mais à `X -> M -> Y`.

La médiation peut être complète ou partielle. Une **médiation complète** est un modèle dans lequel tout l'effet du prédicteur sur la VD "passe" par la médiatrice. Cependant, dans la réalité, ce n'est que rarement le cas : on a généralement affaire à des **médiations partielles**, dans lesquels sont présents à la fois un effet indirect du prédicteur via la médiatrice, et un effet direct du prédicteur sur la variable dépendante.

| **Médiation complète** | **Médiation partielle** |
| --- | --- |
| ![X -> M -> Y](../graphics/sem-mediation-full.png) | ![X -> M -> Y + X -> Y](../graphics/sem-mediation-partial.png) |

Dans une médiation partielle, le calcul des effets se fait de la manière suivante:

- **L'effet indirect** correspond au produit de l'effet du prédicteur sur la médiatrice et de l'effet de la médiatrice sur la VD.
- **L'effet total** correspond à la somme des effets indirects et de l'effet direct.

## Application

```R title="Syntaxe R"
# Soit X le prédicteur, M la médiatrice et Y la VD
med <- 'Y ~ b*M + c*X
  M ~ a*X
  ab := a*b'
m <- sem(
    med,
    data=data,
    estimator="ML",
    missing="fiml",
    se="bootstrap"
)
summary(
    m, ci=TRUE,
    rsquare=TRUE,
    standardized=TRUE
)
```

!!! note "Note"

    Dans cette syntaxe, nous utilisons [la méthode bootstrap](../bootstrap.md) afin d'améliorer l'estimation de l'erreur standard, mais cette utilisation n'est pas obligatoire.

## Exemple

Nous souhaitons calculer les effets du modèle suivant:

<figure markdown>
![X -> W -> V -> Y + X -> Y](../graphics/sem-mediation-example.png){ width="350" }
</figure>

Commençons donc par activer `lavaan` et les autres librairies nécessaires:

```R title="Activation des librairies"
library(lavaan)
library(semPlot)
```

Puis générons nos données:

```R title="Génération des données"
# Génération aléatoire de X
set.seed(20)
N <- 5000
Ux <- rnorm(N)
X <- Ux

# Génération aléatoire de W
Uw <- rnorm(N)
W <- Uw + 0.7*X

# Génération aléatoire de V
Uv <- rnorm(N)
V <- Uv + 0.8*W

# Génération aléatoire de Y
Uy <- rnorm(N)
Y <- Uy + 0.6*V + 0.3*X

# Création d'un dataframe
df <- data.frame(cbind(X, W, V, Y))
```

Ensuite, nous pouvons réaliser notre analyse:

```R title="Analyse du chemin"
med <- 'Y ~ xy*X
  Y ~ vy*V
  V ~ wv*W
  W ~ xw*X
  
  indirect := xw*wv*vy
  total := indirect + xy
'
m <- sem(
  med, data=df,
  estimator="ML",
  missing="fiml",
  se="bootstrap"
)
```

Nous pouvons ensuite représenter graphiquement notre modèle:

```R title="Représentation graphique"
semPaths(
  m, "model", "est",
  curvePivot=T,
  style="lisrel",
  title=T,
  intercepts=F
)
title("Modèle d'équation structurelle", adj=.25)
```

<figure markdown>
![Représentation graphique de notre modèle](../graphics/sem-mediation.png)
</figure>

Puis nous affichons les résultats:

```R title="Affichage des résultats"
summary(m, ci=T, standardized=T, rsquare=T)
```

| **Model Test User Model** | |
| --- | --- |
| **Test statistic** | 0.828 |
| **Degrees of freedom** | 2 |
| **P-value (Chi-square)** | 0.661 |

| **Regressions** | **Estimate** | **Std.Err** | **z-value** | **P(>\|z\|)** | **ci** | **Std.lv** | **Std.all** |
| --- | --- | --- | --- | --- | --- | --- | --- |
| **Y ~ X (xy)** | 0.306 | 0.015 | 20.312 | 0.000 | [0.276,0.336] | 0.306 | 0.215 |
| **Y ~ V (vy)** | 0.599 | 0.011 | 55.178 | 0.000 | [0.578,0.621] | 0.599 | 0.592 |
| **V ~ W (wv)** | 0.806 | 0.012 | 68.837 | 0.000 | [0.782,0.829] | 0.806 | 0.695 |
| **W ~ X (xw)** | 0.707 | 0.014 | 51.183 | 0.000 | [0.680,0.735] | 0.707 | 0.581 |

| **R-Square** | **Estimate** |
| --- | --- |
| Y | 0.500 |
| V | 0.484 |
| W | 0.338 |

| **Defined Parameters** | **Estimate** | **Std.Err** | **z-value** | **P(>\|z\|)** | **ci** | **Std.lv** | **Std.all** |
| --- | --- | --- | --- | --- | --- | --- | --- |
| **indirect** | 0.341 | 0.010 | 33.285 | 0.000 | [0.321,0.361] | 0.341 | 0.239 |
| **total** | 0.647 | 0.016 | 40.052 | 0.000 | [0.615,0.679] | 0.647 | 0.454 |

Nous pouvons conclure que le modèle semble adapté correctement aux données (χ^2^~(2)~ = 0.83, _p_ = .661). L'effet direct de `X` sur `Y` (que nous avons appelé `xy` dans notre modèle) est de 0.306 (_B_ = 0.306, _p_<.001). L'effet indirect de `X` sur `Y` par les médiatrices `W` et `V` est de 0.341 (_B_ = 0.341, _p_<.001). L'effet total, qui est la somme de l'effet direct et indirect, est d'une valeur de 0.647 (_B_ = 0.647, _p_<.001).

!!! note
    Nous pouvons également voir que les relations entre les variables ont des coefficients qui correspondent aux données que nous avons générées: cela signifie que nous avons généré notre modèle correctement.
