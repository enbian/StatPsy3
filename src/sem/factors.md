# SEM avec variables latentes

!!! abstract "Introduction"
    En combinant [analyse du chemin](./introduction.md) et [analyse factorielle confirmatoire](./cfa.md), nous pouvons tester des modèles contenant des relations causales entre plusieurs variables latentes.

## Introduction théorique

Puisque l'analyse factorielle confirmatoire permet de tester le modèle de mesure &mdash; c'est-à-dire les liens entre les variables manifestes et les facteurs latents, il est maintenant possible de combiner cette méthode d'analyse avec les analyses de chemin vues précédemment. En travaillant ainsi, il est possible de tester des modèles dans lesquels l'on infère l'existence de relations causales entres les facteurs latents, que l'on ne peut mesurer qu'à travers les variables manifestes.

|  Graphe | Type de modèle |
| --- | --- |
| ![](../graphics/sem-factors-theory-3.png){ width="350" } | **Analyse de chemin** |
| ![](../graphics/sem-factors-theory-1.png){ width="350" } | **Analyse du modèle de mesure** |
| ![](../graphics/sem-factors-theory-2.png){ width="350" } | **Modèle structurel (avec facteurs latents)** |

## Application

L'analyse se fait donc en deux temps. Tout d'abord, il faut réaliser une analyse factorielle confirmatoire pour tester le modèle de mesure, pour ensuite pouvoir réaliser une analyse de chemin entre les facteurs latents.

```R title="Syntaxe R"
# Soit f1 et f2 des facteurs latents,
# f2 étant dépendant de f1,
# i1, i2 et i3 des variables manifestes dépendantes de f1
# et i4, i5, i6 des variables manifestes dépendantes de f2

# 1) Analyse du modèle de mesure (CFA)
model <- 'f1 =~ i1 + i2 + i3
  f2 =~ i4 + i5 + i6'
fit <- cfa(model, data=data)
summary(fit, fit.measures=T, standardized=T, rsquare=T)

# 2) Analyse du modèle structurel
model <- 'f1 =~ i1 + i2 + i3
  f2 =~ i4 + i5 + i6
  f2 ~ f1'
fit <- sem(model, data=data)
summary(fit, fit.measures=T, standardized=T, rsquare=T)
```

!!! warning "Attention"

    Par défaut, `lavaan` va partir du principe qu'il existe des corrélations entre les facteurs latents si aucune relation causale n'est spécifiée. Pour éviter cela dans un modèle où il n'existe pas de corrélation entre les facteurs latents, il est nécessaire de le spécifier dans le modèle à l'aide des symboles `~~` (utilisés pour spécifier les corrélations avec `lavaan`).

    ```R title="Exemple"
    # Soit f2 et f1 des facteurs indépendants
    model <- 'f1 =~ i1 + i2 + i3
      f2 =~ i4 + i5 + i6

      # On indique que la corrélation entre f2 et f1 est de 0
      # (=pas de corrélation)
      f2 ~~ 0*f1'
    ```

## Exemple

Dans cet exemple, nous souhaitons tester le modèle suivant:

<figure markdown>
![Graphe représentant le modèle que nous souhaitons tester](../graphics/sem-structural-example-1.png)
</figure>

Commençons par activer nos librairies:

```R title="Activation des librairies"
library(lavaan)
library(semPlot)
```

Nous pouvons ensuite générer nos données:

```R title="Génération des données"
set.seed(20)
N <- 20000
X <- rnorm(N)
Y <- rnorm(N) + 0.9*X
Z <- rnorm(N) + 0.8*X + 0.7*Y
df <- data.frame(
  iX1=rnorm(N) + 0.4*X,
  iX2=rnorm(N) + 0.6*X,
  iX3=rnorm(N) + 0.7*X,
  iY1=rnorm(N) + 0.3*Y,
  iY2=rnorm(N) + 0.5*Y,
  iY3=rnorm(N) + 0.2*Y,
  iZ1=rnorm(N) + 0.9*Z,
  iZ2=rnorm(N) + 0.8*Z,
  iZ3=rnorm(N) + 0.4*Z
)
```

Nous pouvons ensuite réaliser une analyse factorielle confirmatoire, afin de tester si le modèle de mesure est adapté aux données. Ici, nous cherchons à tester si le modèle à trois variables latentes est correct, et si les items (variables manifestes) mesurent réellement les facteurs latents que nous inférons:

```R title="Analyse factorielle confirmatoire"
m1 <- 'X =~ iX1 + iX2 + iX3
  Y =~ iY1 + iY2 + iY3
  Z =~ iZ1 + iZ2 + iZ3'
f1 <- cfa(
  m1, data=df,
  missing="fiml", estimator="MLR"
)
```

!!! info "Information"

    Ici, nous n'incluons pas encore les relations causales entres les variables latentes. Nous cherchons simplement à savoir si le cadre théorique est correct &mdash; en d'autres termes, à savoir si les variables latentes sont bel et bien mesurées par les variables manifestes.

Une fois l'analyse réalisée, nous pouvons représenter graphiquement le modèle de mesure que nous testons:

```R title="Représentation graphique du modèle de mesure"
semPaths(
  f1, "model", "est",
  curvePivot=F, style="lisrel",
  intercepts=F, title=T,
  layout="tree2"
)
title("Modèle de mesure", adj=0)
```

<figure markdown>
![Représentation graphique du modèle de mesure](../graphics/sem-structural-example-2.png)
</figure>

Interprétons maintenant les résultats de notre analyse factorielle confirmatoire:

```R title="Affichage des résultats"
summary(f1, fit.measures=T, standardized=T, rsquare=T, ci=T)
```

| **Model Test User Model** | **Standard** | **Scaled** |
| --- | --- | --- |
| **Test Statistic** | 22.424 | 22.262 |
| **Degrees of freedom** | 24 | 24 |
| **P-value (Chi-square)** | 0.554 | 0.564 |
| **Scaling correction factor** | | 1.007 |

| **Model Test Baseline Model** | **Standard** | **Scaled** |
| --- | --- | --- |
| Test Statistic | 37190.509 | 36980.535 |
| Degrees of freedom | 36 | 36 |
| P-value | 0.000 | 0.000 |
| Scaling correction factor | | 1.006 |

| **User Model versus Baseline Model** | **Standard** | **Scaled** |
| --- | --- | --- |
| Comparative Fit Index (CFI) | 1.000 | 1.000 |
| Tucker-Lewis Index (TLI) | 1.000 | 1.000 |
| Robust Comparative Fit Index (CFI) | | 1.000 |
| Robust Comparative Fit Index (CFI) | | 1.000 |

| **Loglikelihood and Information Criteria** | **Standard** | **Scaled** |
| --- | --- | --- |
| Loglikelihood user model (H0) | -280819.679 | -280819.679 |
| Scaling correction factor | | 1.000 |
| Loglikelihood unrestricted model (H1) | -280808.467 | -280808.467 |
| Scaling correction factor | | 1.003 |
| Akaike (AIC) | 561699.359 | 561699.359 |
| Bayesian (BIC) | 561936.463 | 561936.463 |
| Sample-size adjusted Bayesian (SABIC) | 561841.125 | 561841.125 |

| **Root Mean Square Error of Approximation** | **Standard** | **Scaled** |
| --- | --- | --- |
| RMSEA | 0.000 | 0.000 |
| 90 Percent confidence interval - lower | 0.000 | 0.000 |
| 90 Percent confidence interval - upper | 0.005 | 0.005 |
| P-value H_0: RMSEA <= 0.050 | 1.000 | 1.000 |
| P-value H_0: RMSEA >= 0.080 | 0.000 | 0.000 |
| Robust RMSEA | | 0.000 |
| 90 Percent confidence interval - lower | | 0.000 |
| 90 Percent confidence interval - upper | | 0.005 |
| P-value H_0: Robust RMSEA <= 0.050 | | 1.000 |
| P-value H_0: Robust RMSEA >= 0.080 | | 0.000 |

| **Standardized Root Mean Square Residual** | **Standard** | **Scaled** |
| --- | --- | --- |
| SRMR | 0.003 | 0.003 |

| **Latent Variables** | **Estimate** | **Std.Err** | **z-value** | **P(>\|z\|)** | **ci** | **Std.lv** | **Std.all** |
| --- | --- | --- | --- | --- | --- | --- | --- |
| **X =~ iX1** | 1.000 | | | | [1.000,1.000] | 0.381 | 0.357 |
| **X =~ iX2** | 1.604 | 0.045 | 35.358 | 0.000 | [1.515,1.693] | 0.612 | 0.526 |
| **X =~ iX3** | 1.839 | 0.050 | 36.907 | 0.000 | [1.742,1.937] | 0.701 | 0.580 |
| **Y =~ iY1** | 1.000 | | | | [1.000,1.000] | 0.414 | 0.382 |
| **Y =~ iY2** | 1.657 | 0.044 | 37.903 | 0.000 | [1.571,1.743] | 0.686 | 0.562 |
| **Y =~ iY3** | 0.627 | 0.025 | 24.939 | 0.000 | [0.577,0.676] | 0.259 | 0.252 |
| **Z =~ iZ1** | 1.000 | | | | [1.000,1.000] | 1.693 | 0.861 |
| **Z =~ iZ2** | 0.892 | 0.008 | 118.954 | 0.000 | [0.878,0.907] | 1.510 | 0.835 |
| **Z =~ iZ3** | 0.444 | 0.005 | 86.843 | 0.000 | [0.434,0.454] | 0.752 | 0.604 |

Sur la base des résultats de cette analyse, nous pouvons affirmer que notre modèle de mesure est adapté aux données (χ^2^~(24)~ = 22.42, _p_ = .554, CFI = 1, RMSEA = 0, SRMR = 0.003). Les différentes charges factorielles semblent également correspondre plus ou moins à ce que nous avions généré.

Maintenant que notre modèle de mesure est validé, nous pouvons maintenant passer à l'analyse structurelle du modèle, c'est-à-dire à l'analyse des relations causales entre les variables latentes:

```R title="Modèle structurel"
m2 <- 'X =~ iX1 + iX2 + iX3
  Y =~ iY1 + iY2 + iY3
  Z =~ iZ1 + iZ2 + iZ3

  Z ~ xz*X
  Y ~ xy*X
  Z ~ yz*Y
  
  indirect := xy*yz
  total := xz+indirect'
f2 <- sem(
  m2, data=df,
  missing="fiml", estimator="MLR"
)
```

Nous pouvons à nouveau représenter le modèle &mdash; cette fois-ci contenant les relations structurelles &mdash; dans un graphe:

```R title="Représentation graphique du modèle structurel"
semPaths(
  f2, "model", "est",
  curvePivot=F, style="lisrel",
  intercepts=F, title=T,
  layout="tree2"
)
title("Modèle structurel", adj=0)
```

<figure markdown>
![Représentation graphique du modèle structurel](../graphics/sem-structural-example-3.png)
</figure>

Finalement, nous pouvons analyser les résultats de l'analyse du modèle structurel:

```R title="Affichage des résultats"
summary(f2, fit.measures=T, standardized=T, rsquare=T, ci=T)
```

| **Model Test User Model** | **Standard** | **Scaled** |
| --- | --- | --- |
| **Test Statistic** | 22.424 | 22.262 |
| **Degrees of freedom** | 24 | 24 |
| **P-value (Chi-square)** | 0.554 | 0.564 |
| **Scaling correction factor** | | 1.007 |

| **Regressions** | **Estimate** | **Std.Err** | **z-value** | **P(>\|z\|)** | **ci** | **Std.lv** | **Std.all** |
| --- | --- | --- | --- | --- | --- | --- | --- |
| **Z ~ X (xz)** | 1.743 | 0.106 | 16.500 | 0.000 | [1.536,1.950] | 0.393 | 0.393 |
| **Y ~ X (xy)** | 0.732 | 0.028 | 26.076 | 0.000 | [0.677,0.787] | 0.675 | 0.675 |
| **Z ~ Y (yz)** | 2.149 | 0.113 | 19.044 | 0.000 | [1.927,2.370] | 0.525 | 0.525 |

| **Defined Parameters** | **Estimate** | **Std.Err** | **z-value** | **P(>\|z\|)** | **ci** | **Std.lv** | **Std.all** |
| --- | --- | --- | --- | --- | --- | --- | --- |
| **indirect** | 1.574 | 0.090 | 17.460 | 0.000 | [1.397,1.750] | 0.354 | 0.354 |
| **total** | 3.317 | 0.091 | 36.556 | 0.000 | [3.139,3.495] | 0.747 | 0.747 |

Ici, les charges factorielles des liens entre variables manifestes et variables latentes n'ont pas changé. Le modèle non plus n'a pas changé, il reste adapté aux données (χ^2^~(24)~ = 22.42, _p_ = .554, CFI = 1, RMSEA = 0, SRMR = 0.003).

Nous constatons que toutes les coefficients de régression entre les variables latentes ainsi que les effets indirects et totaux sont tous significatifs (_p_<.001). Ceux-ci sont plus élevés que les relations causales réelles, mais cela s'explique par le fait que l'analyse ne peut évaluer ces dernières qu'à travers les variables manifestes.
