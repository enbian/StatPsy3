# Analyse factorielle confirmatoire (CFA)

!!! abstract "Introduction"
    L'analyse factorielle confirmatoire (AFC, ou _CFA_ en anglais) est une analyse statistique permettant de tester si un modèle est adapté aux données. Il s'agit d'une technique de modélisation d'équation structurelle (SEM), utilisée spécifiquement pour tester des modèles de mesures &mdash; c'est-à-dire des modèles utilisant des variables manifestes pour mesurer des facteurs latents.

## Introduction théorique

En psychologie, les processus psychologiques (comme la dépression) sont impossibles à mesurer directement. Ces construits théoriques, lorsqu'ils sont testés statistiquement, sont appelés des **variables latentes** &mdash; c'est-à-dire que l'on ne peut pas les observer directement. À la place, on les observe à travers les symptômes que l'on peut observer, ou bien à travers les scores de différentes échelles, qui sont des **variables manifestes** (ou observables). De manière générale, on infère que les variations des variables latentes sont à l'origine des variations des variables manifestes. C’est de cette manière que fonctionnent la plupart des questionnaires psychométriques.

Pour représenter graphiquement un modèle SEM, nous utilisons des carrés/rectangles pour représenter les variables manifestes, et des cercles pour représenter les variables latentes:

<figure markdown>
![Variable latente F qui influence trois variables manifestes i1, i2 et i3](../graphics/cfa-theory-1.png)

Dans le modèle ci-dessus, `F` est la variable latente qui influence les variables manifestes `i1`, `i2` et `i3`. Nous ne pouvons pas mesurer `F` directement, nous devons donc passer par `i1`, `i2` et `i3` pour le faire.
</figure>

L'analyse factorielle confirmatoire permet de déterminer si les relations causales entre les variables manifestes et les variables latentes &mdash; dont l'existence est inférée &mdash; existent dans les données sur lesquelles nous réalisons l'analyse, sur la base des corrélations entre les variables manifestes censées évaluer les mêmes facteurs latents.

## Échelles de mesure

Pour mesurer les variables latentes, il faut donc passer par des échelles, qui sont des variables manifestes. Ces échelles sont composées de différents items (généralement des questions). Idéalement, ceux-ci ne doivent pas être trop spécifiques à un domaine.

!!! warning "Attention"

    Il est important de tenir compte des variables latentes utilisées. En effet, si l'on utilise le conflit (variable manifeste) pour évaluer la satisfaction relationnelle (variable latente), alors il ne sera pas possible de tester si le conflit impact la satisfaction relationnelle. En effet, on trouvera une corrélation forte, mais qui est due uniquement au fait que nous mesurons deux fois la même chose. Dans ce cas, on dit que le prédicteur et la VD sont confondues.

Les items ne peuvent pas être des prédicteurs, puisque plusieurs items sont nécessaire pour construire une échelle qui évalue une seule chose. Le score d'une échelle correspond à la moyenne des items (ce qui permet de travailler avec des valeurs manquantes). Avec R, il est possible de calculer la moyenne de la manière suivante:

```R title="Syntaxe R"
# Soit i1, i2 et i3 des items 
# et m la moyenne de ces derniers
data$m <- rowMeans(data[,c("i1", "i2", "i3")], na.rm=TRUE)
```

Il est également nécessaire que les items soient clairs, car si le concept n'est pas mesuré correctement, alors le résultat statistique ne peut pas être interprêté. Parfois, il est nécessaire de recoder des items négatifs pour que ceux-ci soient comparables avec d'autres items positifs. Avec R:

```R title="Syntaxe R"
# Ici pour recoder un item sur une échelle de 1-5
data$item.r <- 6 - data$item
```

## Approche exploratoire/confirmatoire

Il existe deux approches d'analyse : l'une est exploratoire et l'autre est confirmatoire. L'analyse factorielle exploratoire  (AFE ou _EFA_ en anglais) n'est pas abordée dans le cadre de ce cours, mais elle consiste à réaliser une analyse sans théorie. En réalisant une matrice de corrélations, elle vise à identifier les items qui corrèlent fortement entre eux, avec l'idée que la cause de cette corrélation est le fait que les items mesurent la même chose.

Dans l'approche confirmatoire, on a déjà une théorie, que l'on souhaite confirmer. L'analyse factorielle confirmatoire utilise à nouveau les corrélations entre les items, mais cette fois-ci pour confirmer le modèle théorique.

## Conditions d'application

!!! success "Conditions d'application"

    Pour pouvoir être appliqué, l'analyse factorielle nécessite:

    - Un nombre de degrés de libertés **supérieur ou égal à 0**
    - Si le modèle n'a qu'un facteur, il nécessite **au minimum 3 variables manifestes**.
    - Si le modèle a plusieurs facteurs, il nécessite **au minimum 2 variables manifestes**.
    - **La première charge factorielle doit être fixée à 1** (cela se fait automatiquement dans `lavaan`).

## Application

```R title="Syntaxe R"
# Soit f1 et f2 des facteurs latents
# i1, i2 et i3 des variables manifestes dépendantes de f1
# et i4, i5, i6 des variables manifestes dépendantes de f2

model <- 'f1 =~ i1 + i2 + i3
  f2 =~ i4 + i5 + i6'
fit <- cfa(model, data=data)
summary(fit, fit.measures=T, standardized=T, rsquare=T)
```

!!! note "Note"
    Avec `lavaan`, nous définissons les variables latentes à l'aide des symboles `=~`.
  
## Interprétation

Pour évaluer l'adéquation du modèle avec les données, nous utilisons quatre indices d'ajustement.

- La **p-valeur** calculée à partir du **khi-carré**
- La **RMSEA** (Root Mean Square Error of Approximation)
- La **SRMR** (Standardized Root Mean Square Residual)
- Le **CFI** (Comparative Fit Index)

| **Indice** | **Bon ajustement du modèle** | **Mauvais ajustement du modèle** |
| --- | --- | --- |
| **χ^2^** | p >= 0.05 | p < 0.05 |
| **RMSEA** | RMSEA <= 0.06 | RMSEA > 0.06 |
| **SRMR** | SRMR <= 0.10 | SRMR > 0.10 |
| **CFI** | CFI >= 0.95 | CFI < 0.95 |

## Exemple

Nous souhaitons mesurer trois variables latentes `X`, `Y` et `Z`. Nous faisons l'hypothèse que des items permettent d'évaluer ces variables. Nous avons donc trois items par variable latente. Les valeurs de ces items représentent donc des variables manifestes, que nous utilisons pour mesurer `X`, `Y` et `Z`.

Nous voulons donc vérifier que notre modèle théorique correspond bel et bien aux données.

Commençons par importer les librairies nécessaires:

```R title="Activation des librairies"
library(lavaan)
library(semPlot)
```

Puis nous pouvons générer aléatoirement les données:

```R title="Génération des données"
set.seed(20)
N <- 20000
X <- rnorm(N)
Y <- rnorm(N)
Z <- rnorm(N)
df <- data.frame(
  iX1=rnorm(N) + 0.4*X,
  iX2=rnorm(N) + 0.6*X,
  iX3=rnorm(N) + 0.7*X,
  iY1=rnorm(N) + 0.3*Y,
  iY2=rnorm(N) + 0.5*Y,
  iY3=rnorm(N) + 0.2*Y,
  iZ1=rnorm(N) + 0.9*Z,
  iZ2=rnorm(N) + 0.8*Z,
  iZ3=rnorm(N) + 0.4*Z
)
```

!!! question "Pourquoi n'incluons-nous pas X, Y et Z dans le dataframe?"
    Nous n'incluons pas les variables latentes dans le dataframe, puisque celles-ci ne peuvent pas être mesurées. Nous connaissons leur valeur uniquement parce que nous les avons utilisées pour générer les valeurs des variables manifestes.

    Dans la réalité &mdash; où nous ne pouvons pas générer les données &mdash; nous n'aurions pas accès à la valeur des variables latentes.

Réalisons maintenant notre analyse:

```R title="Analyse factorielle confirmatoire"
model <- 'X =~ iX1 + iX2 + iX3
  Y =~ iY1 + iY2 + iY3
  Z =~ iZ1 + iZ2 + iZ3'
fit <- cfa(
  model, data=df,
  missing="fiml", estimator="MLR"
)
```

Nous pouvons ensuite visualiser notre modèle:


```R title="Représentation graphique du modèle"
semPaths(
  fit, "model", "est",
  curvePivot=T, style="lisrel",
  title=T, intercepts=F
)
title(
  "Analyse factorielle confirmatoire",
  adj=.25
)
```

<figure markdown>
![Représentation graphique du modèle](../graphics/cfa-mesure-example.png)
</figure>

Interprétons maintenant nos résultats:

```R title="Affichage des résultats"
summary(fit, fit.measures=T, standardized=T, rsquare=T)
```

| **Model Test User Model** | **Standard** | **Scaled** |
| --- | --- | --- |
| **Test Statistic** | 20.190 | 20.059 |
| **Degrees of freedom** | 24 | 24 |
| **P-value (Chi-square)** | 0.686 | 0.693 |
| **Scaling correction factor** | | 1.007 |

| **Model Test Baseline Model** | **Standard** | **Scaled** |
| --- | --- | --- |
| Test Statistic | 9526.278 | 9477.661 |
| Degrees of freedom | 36 | 36 |
| P-value | 0.000 | 0.000 |
| Scaling correction factor | | 1.005 |

| **User Model versus Baseline Model** | **Standard** | **Scaled** |
| --- | --- | --- |
| Comparative Fit Index (CFI) | 1.000 | 1.000 |
| Tucker-Lewis Index (TLI) | 1.001 | 1.001 |
| Robust Comparative Fit Index (CFI) | | 1.000 |
| Robust Comparative Fit Index (CFI) | | 1.001 |

| **Loglikelihood and Information Criteria** | **Standard** | **Scaled** |
| --- | --- | --- |
| Loglikelihood user model (H0) | -274785.361 | -274785.361 |
| Scaling correction factor | | 1.001 |
| Loglikelihood unrestricted model (H1) | -274775.266 | -274775.266 |
| Scaling correction factor | | 1.004 |
| Akaike (AIC) | 549630.722 | 549630.722 |
| Bayesian (BIC) | 549867.826 | 549867.826 |
| Sample-size adjusted Bayesian (SABIC) | 549772.488 | 549772.488 |

| **Root Mean Square Error of Approximation** | **Standard** | **Scaled** |
| --- | --- | --- |
| RMSEA | 0.000 | 0.000 |
| 90 Percent confidence interval - lower | 0.000 | 0.000 |
| 90 Percent confidence interval - upper | 0.005 | 0.005 |
| P-value H_0: RMSEA <= 0.050 | 1.000 | 1.000 |
| P-value H_0: RMSEA >= 0.080 | 0.000 | 0.000 |
| Robust RMSEA | | 0.000 |
| 90 Percent confidence interval - lower | | 0.000 |
| 90 Percent confidence interval - upper | | 0.005 |
| P-value H_0: Robust RMSEA <= 0.050 | | 1.000 |
| P-value H_0: Robust RMSEA >= 0.080 | | 0.000 |

| **Standardized Root Mean Square Residual** | **Standard** | **Scaled** |
| --- | --- | --- |
| SRMR | 0.004 | 0.004 |

| **Latent Variables** | **Estimate** | **Std.Err** | **z-value** | **P(>\|z\|)** | **Std.lv** | **Std.all** |
| --- | --- | --- | --- | --- | --- | --- |
| **X =~ iX1** | 1.000 | | | | 0.372 | 0.348 |
| **X =~ iX2** | 1.605 | 0.059 | 26.997 | 0.000 | 0.597 | 0.513 |
| **X =~ iX3** | 1.952 | 0.081 | 24.085 | 0.000 | 0.726 | 0.600 |
| **Y =~ iY1** | 1.000 | | | | 0.324 | 0.310 |
| **Y =~ iY2** | 1.490 | 0.212 | 7.025 | 0.000 | 0.483 | 0.428 |
| **Y =~ iY3** | 0.582 | 0.059 | 9.876 | 0.000 | 0.188 | 0.186 |
| **Z =~ iZ1** | 1.000 | | | | 0.912 | 0.675 |
| **Z =~ iZ2** | 0.886 | 0.027 | 32.542 | 0.000 | 0.808 | 0.629 |
| **Z =~ iZ3** | 0.446 | 0.014 | 32.871 | 0.000 | 0.407 | 0.380 |

Notre modèle semble adapté aux données (χ^2^~(24)~ = 20.19, _p_ = .686, CFI = 1, RMSEA = 0, SRMR = 0.004).

Si nous regardons les charges factorielles &mdash; les coefficients de régression qui représentent les relations entre les variables manifestes et les facteurs latents &mdash; nous constatons également que celles-ci correspondent assez bien aux données que nous avions généré.
