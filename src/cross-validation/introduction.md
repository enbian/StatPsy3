# Séparation des données

## Introduction

En machine learning, l'un des problèmes principaux vient du fait que les données disponibles doivent être utilisées pour entraîner le modèle, mais également pour le tester. Or, si les mêmes données sont utilisées à la fois pour entraîner le modèle et pour le tester, le test n'a aucun sens. En effet, le modèle ne _prédit_ rien ; il ne fait que répéter des informations qu'il a déjà traitées.

Pour cette raison, il est commun de fractionner les données disponibles (c'est-à-dire les séparer en deux parties), et d'en utiliser une partie pour entraîner le modèle, et une autre partie pour le tester.

## Application

```R title="Syntaxe R"
# Soit y la variable dépendante,
# et data le dataframe contenant les données
library(caret)
parts <- createDataPartition(y = data$y, p=.5, list=F) # ici 50/50%
data_training <- data[parts,]
data_testing <- data[-parts,]
```

## Problème

Cependant, cette méthode à un inconvénient : elle réduit la taille de l'échantillon sur lequel le modèle est entraîné. Or, pour que le modèle puisse être performant, il est nécessaire que ce dernier soit entraîné sur de grands échantillons, afin d'augmenter la puissance statistique. La solution à ce problème est **la validation croisée**.
