# Validation croisée K-fold (K-fold cross validation)

!!! abstract "Introduction"

    La validation croisée K-fold permet d'augmenter la puissance statistique des données utilisées pour entraîner et tester le modèle.

## Introduction théorique

Au lieu de simplement séparer les données et d'en utiliser une partie pour entraîner le modèle, et une autre partie pour le tester, la validation croisée K-fold permet d'utiliser toutes les données, à la fois pour l'entraînement et pour la procédure de test.

1. La méthode consiste à dupliquer le tableau de données initiales plusieurs fois (_K_ étant le nombre de fois que le tableau est copié). Souvent, on le fait entre 3 à 10 fois. 
1. Puis, chaque tableau est divisé en autant de parties qu'il existe de copies du tableau. 
1. Ensuite, pour chaque tableau, on définit une partie qui va être utilisée pour la procédure de test. Cette partie est différente pour tous les tableaux. Toutes les autres parties seront utilisées pour l'entraînement.
1. Au final, toutes les parties seront donc utilisées `K-1` fois pour l'entraînement, et `1` fois pour le test.

<figure markdown>
![K-fold cross validation](../graphics/k-fold-theory.png){ width="700" }
</figure>

## Application

| :octicons-file-directory-open-fill-16: Télécharger la fonction |
| --- |
| [:simple-r: k-fold-function-src.R](../data/k-fold-function-src.R) |

```R title="Syntaxe R"
# Soit k le nombre de duplications à effectuer,
# y la variable dépendante,
# df le dataframe contenant les données,
# et DV le nom de la colonne de la variable dépendante (ici "y")

library(party)
library(caret)
library(randomForest)
library(dplyr)

cross_validation <- function(data, k, DV, rforest=F) {
  names(data)[names(data) == DV] = "dependent"
  data <- mutate(data, kfolds=sample(
    1:k, size=nrow(data), replace=TRUE
  ))
  results <- data.frame()
  for(fold in 1:k){
    train <- data[data$kfolds != fold,][1:dim(data)[2]-1]
    test <- data[data$kfolds == fold,][1:dim(data)[2]-1]
    if(!rforest) {
      m <- ctree(dependent ~ ., data=train)
    }
    else {
      m <- randomForest(dependent ~ ., data=train)
    }
    pred <- predict(
      m, type="response", newdata=test
    )
    results <- rbind(
      data.frame(predictions=pred, reality=test$dependent),
      results
    )
  }
  return(results)
}

test_results <- cross_validation(df, k=5, DV="y")
```

## Exemple

| :octicons-file-directory-open-fill-16: Fichiers |
| --- |
| [:fontawesome-solid-file-csv: k-fold.csv](../data/k-fold.csv) |
| [:simple-r: k-fold.R](../data/k-fold.R) |
| [:simple-r: k-fold-function-src.R](../data/k-fold-function-src.R) |

Commençons par activer le fichier source qui contient la fonction nécessaire:

```R title="Importation du fichier source"
source("k-fold-function-src.R")
```

Importons maintenant nos données:

```R title="Importation et recodage des données"
df <- read.csv("k-fold.csv")
df$Y <- as.factor(df$Y)
```

Puis construisons l'arbre de décision:

```R title="Prédictions (arbre de décision)"
dt_results <- cross_validation(df, 5, DV="Y")
```

Visualisons nos résultats dans une matrice de confusion:

```R title="Matrice de confusion"
confusionMatrix(
  data=dt_results$predictions,
  reference=dt_results$reality
)
```

| | **Ref A** | **Ref B** | **Ref C** | **Ref D** |
| --- | --- | --- | --- | --- |
| **Pred A** | 260 | 35 | 0 | 0 |
| **Pred B** | 36 | 623 | 52 | 0 |
| **Pred C** | 0 | 42 | 595 | 41 |
| **Pred D** | 0 | 0 | 36 | 280 |

| **Overall Statistics** | |
| --- | --- |
| **Accuracy** | 0.879 |
| **95% CI** | (0.8639, 0.893) |
| **No Information Rate** | 0.35 |
| **P-Value [Acc > NIR]** | < 2.2e-16 |
| **Kappa** | 0.8302 |
| **Mcnemar's Test P-Value** | NA |

Nous pouvons voir que le modèle a une exactitude de `0.879` (95% CI = [0.8639, 0.893]). Nous voulons maintenant utiliser une forêt aléatoire afin de consolider notre algorithme:

```R title="Prédictions (forêt aléatoire)"
rf_results <- cross_validation(df, 5, DV="Y", rforest=T)
```

Nous pouvons à nouveau visualiser nos résultats dans une matrice de confusion:

```R title="Matrice de confusion"
confusionMatrix(
  data=rf_results$predictions, 
  reference=rf_results$reality
)
```

| | **Ref A** | **Ref B** | **Ref C** | **Ref D** |
| --- | --- | --- | --- | --- |
| **Pred A** | 272 | 17 | 0 | 0 |
| **Pred B** | 24 | 638 | 42 | 0 |
| **Pred C** | 0 | 45 | 621 | 29 |
| **Pred D** | 0 | 0 | 20 | 292 |

| **Overall Statistics** | |
| --- | --- |
| **Accuracy** | 0.9115 |
| **95% CI** | (0.8982, 0.9236) |
| **No Information Rate** | 0.35 |
| **P-Value [Acc > NIR]** | < 2.2e-16 |
| **Kappa** | 0.8757 |
| **Mcnemar's Test P-Value** | NA |

Le modèle possède maintenant une exactitude de `0.9115` (95% CI = [0.8982, 0.9236]), ce qui signifie qu'il est plutôt bon pour prédire notre variable dépendante.

